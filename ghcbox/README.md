#### Where it is

    docker pull lykrysh/ghcbox

#### What's in it

* alpine
* ghc
* cabal
* tmux
* vim
set nocp
source /root/.vim/autoload/pathogen.vim
call pathogen#infect()
syntax on
filetype plugin indent on

set nocompatible
set number
set nowrap
set showmode
set tw=80
set smartcase
set smarttab
set smartindent
set autoindent
set softtabstop=2
set shiftwidth=2
set expandtab
set incsearch
set mouse=a
set history=1000
set clipboard=unnamedplus,autoselect
set completeopt=menuone,menu,longest

set wildignore+=*\\tmp\\*,*.swp,*.swo,*.zip,.git,.cabal-sandbox
set wildmode=longest,list,full
set wildmenu
set completeopt+=longest
set t_Co=256
set cmdheight=1

""""""""""""""""""""""
" Syntastic
""""""""""""""""""""""
map <Leader>s :SyntasticToggleMode<CR>

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 0
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 0

"""""""""""""""""""""""
set lcs=tab:#\ ,trail:-
set t_md=
set noro
set background=dark
set laststatus=2
set statusline=%F%m%r%h%w%y/%l/%L
set autoread
set autowrite
set list
"set autochdir
autocmd BufEnter * lcd %:p:h
set makeef=make.err
set hlsearch
set confirm
set backspace=indent,eol,start
set ruler               " show the cursor position all the time
set showcmd             " display incomplete commands
set vb t_vb=            " shut off bell entirely; see also .gvimrc
set incsearch           " do incremental searching
set nowarn              " don't warn for shell command when buffer changed
set si

" hi Normal ctermfg=white cterm=NONE
hi NonText ctermfg=black
" hi Folded ctermfg=white cterm=NONE
" hi Constant ctermfg=white cterm=NONE
" hi Title ctermfg=white cterm=NONE
" hi Ignore ctermfg=white cterm=NONE
" hi Underlined ctermfg=white cterm=NONE
" hi Statement ctermfg=white cterm=NONE
" hi Comment ctermfg=blue
" hi Exception ctermfg=white
" hi Type ctermfg=white cterm=NONE
" hi Special ctermfg=white cterm=NONE
" hi Visual  ctermfg=black ctermbg=white  cterm=NONE
hi Error ctermfg=yellow ctermbg=black
" hi Identifier ctermfg=white cterm=NONE
" hi PreProc ctermfg=white cterm=NONE
hi LineNr ctermfg=magenta ctermbg=black
" hi ModeMsg ctermfg=magenta ctermbg=black
hi StatusLine ctermfg=white ctermbg=magenta cterm=NONE
" hi StatusLineNC ctermfg=white ctermbg=black cterm=NONE
" hi VertSplit ctermfg=magenta ctermbg=NONE cterm=NONE
" if version >= 700
au InsertEnter * hi StatusLine ctermfg=white ctermbg=red cterm=NONE
au InsertLeave * hi StatusLine ctermfg=white ctermbg=magenta cterm=NONE
" endif
" hi MoreMsg  ctermfg=yellow
" hi ErrorMsg ctermfg=brown
" hi MatchParen ctermfg=magenta ctermbg=black cterm=NONE
" hi Question ctermfg=yellow
" hi WarningMsg   ctermfg=cyan
"
" split vertically
let g:explVertical=1

" fix $(...) error
let g:is_posix = 1

"""""""""""""""""""""""""

set nobackup
set backupdir=~/.vimdumpster/
set backupskip=~/.vimdumpster/*
set directory=~/.vimdumpster/
set writebackup

""""""""""""""""""""""""

nnoremap <C-H> :Hexmode<CR>
inoremap <C-H> <Esc>:Hexmode<CR>
vnoremap <C-H> :<C-U>Hexmode<CR>

" ex command for toggling hex mode - define mapping if desired
command -bar Hexmode call ToggleHex()

" helper function to toggle hex mode
function ToggleHex()
  " hex mode should be considered a read-only operation
  " save values for modified and read-only for restoration later,
  " and clear the read-only flag for now
  let l:modified=&mod
  let l:oldreadonly=&readonly
  let &readonly=0
  let l:oldmodifiable=&modifiable
  let &modifiable=1
  if !exists("b:editHex") || !b:editHex
    " save old options
    let b:oldft=&ft
    let b:oldbin=&bin
    " set new options
    setlocal binary " make sure it overrides any textwidth, etc.
    silent :e " this will reload the file without trickeries 
              "(DOS line endings will be shown entirely )
    let &ft="xxd"
    " set status
    let b:editHex=1
    " switch to hex editor
    %!xxd
  else
    " restore old options
    let &ft=b:oldft
    if !b:oldbin
      setlocal nobinary
    endif
    " set status
    let b:editHex=0
    " return to normal editing
    %!xxd -r
  endif
  " restore values for modified and read only state
  let &mod=l:modified
  let &readonly=l:oldreadonly
  let &modifiable=l:oldmodifiable
endfunction

"""""""""""""""""""""""""

au FileType haskell nnoremap <buffer> <F1> :HdevtoolsType<CR>
au FileType haskell nnoremap <buffer> <silent> <F2> :HdevtoolsClear<CR>

map <F3> : call CleanSwapFile()<CR>
func! CleanSwapFile()
	exec "!rm -rf ~/.vimdumpster/*%<*.swp"
endfunc

""""""""""""""""""""""""

nnoremap <silent><Up> :set paste<CR>m`o<Esc>``:set nopaste<CR>
nnoremap <silent><Down> :set paste<CR>m`O<Esc>``:set nopaste<CR>

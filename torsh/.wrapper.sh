export PS1='\e[45mtorsh \w $\e[0m '

alias wget='torsocks wget'
alias curl='torsocks curl'
alias nmap='torsocks nmap'
alias nc='torsocks nc'
alias nc.openbsd='torsocks nc.openbsd'
alias q='exit'
#### Where it is

    docker pull lykrysh/torsh

#### What's in it

* ubuntu
* tor
* privoxy
* python2
* python3
* torrequest
* pwntools
* billcipher
* nmap ... and more

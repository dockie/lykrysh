#### Where it is

    docker pull lykrysh/lapisbox

#### What's in it

* alpine
* openresty
* lua
* luarocks
* lpeg
* lapis

Forked from: [openresty/docker-openresty](https://github.com/openresty/docker-openresty)
